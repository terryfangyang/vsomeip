#include <iomanip>
#include <iostream>
#include <fstream>
#include <sstream>



#include <vsomeip/vsomeip.hpp>

#define SAMPLE_SERVICE_ID 0x1234
#define SAMPLE_INSTANCE_ID 0x5678
#define SAMPLE_METHOD_ID 0x0421



std::shared_ptr<vsomeip::application> app;

void on_message(const std::shared_ptr<vsomeip::message> &_request) {

    std::shared_ptr<vsomeip::payload> its_payload = _request->get_payload();
    vsomeip::length_t l = its_payload->get_length();

    // Get payload
    char c = *its_payload->get_data();
    std::cout << "\nServer: Received message : "<< c<<"\n\n";

    // Create response
    std::shared_ptr<vsomeip::message> its_response = vsomeip::runtime::get()->create_response(_request);
    its_payload = vsomeip::runtime::get()->create_payload();
    std::vector<vsomeip::byte_t> its_payload_data;
    if(c=='s'){
        its_payload_data.push_back('s');//string
        std::string s = "some string\n";
        for(int i=0; i<s.size(); i++)
        its_payload_data.push_back(s[i]);
    }

    else if(c=='p'){
        its_payload_data.push_back('p');//picture
        std::ifstream infile("../../img/parrot.ppm", std::ifstream::binary);
        std::vector<char> buffer((std::istreambuf_iterator<char>(infile)), (std::istreambuf_iterator<char>()));
        for(int i=0; i<buffer.size(); i++)
        its_payload_data.push_back(buffer[i]);
    }

    else{
        its_payload_data.push_back('s');
        std::string s = "invalid input\n";
        for(int i=0; i<s.size(); i++)
        its_payload_data.push_back(s[i]);
    }

    its_payload->set_data(its_payload_data);
    its_response->set_payload(its_payload);
    app->send(its_response, true);
}


int main(int argc, char **argv) {


   app = vsomeip::runtime::get()->create_application("World");
   app->init();
   app->register_message_handler(SAMPLE_SERVICE_ID, SAMPLE_INSTANCE_ID, SAMPLE_METHOD_ID, on_message);
   app->offer_service(SAMPLE_SERVICE_ID, SAMPLE_INSTANCE_ID);
   app->start();

}